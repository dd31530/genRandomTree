#!/usr/bin/ksh

function randomNumber
{
  typeset -i RANGE=$1
  return $(echo "$RANDOM%$RANGE+1" | bc)
}

function randomTarget
{
  typeset -i DIR_COUNT
  DIR_COUNT=$(find $ROOT_TREE -type d | wc -l)
  randomNumber $DIR_COUNT
  LINE=$?
  TARGET=$(find $ROOT_TREE -type d | sed -ne "$LINE p")
}

function buildFile
{
  NEW_FILE="${FILE_PREFIX}_${OBJ_COUNT}"
  randomTarget
  echo "construction fichier $NEW_FILE dans $TARGET"
  echo "chemin:    $TARGET"   >  $TARGET/$NEW_FILE
  echo "fichier:   $NEW_FILE" >> $TARGET/$NEW_FILE
}

function buildDir
{
  NEW_DIR="${DIR_PREFIX}_${OBJ_COUNT}"
  randomTarget
  echo "construction répertoire $NEW_DIR dans $TARGET"
  mkdir $TARGET/$NEW_DIR
}

function buildObject
{
  let OBJ_COUNT+=1
  randomNumber 2
  case $? in
    1) buildDir
       ;;
    2) buildFile
       ;;
  esac
}

function treeStatus
{
  OBJ_COUNT=$(find $ROOT_TREE | grep -v "^${ROOT_TREE}$" | wc -l)
  DIR_COUNT=$(find $ROOT_TREE -type d | grep -v "^${ROOT_TREE}$" | wc -l)
  FILE_COUNT=$(find $ROOT_TREE -type f | wc -l)

  echo "il y a $OBJ_COUNT objets dans $ROOT_TREE:"
  echo " $DIR_COUNT répertoire(s)"
  echo " $FILE_COUNT fichier(s)"
}

# main

[ $# -ne 2 ] && echo "Usage: $(basename $0) DIR_NAME OBJECTS_COUNT" && exit 1

ROOT_TREE=$1
OBJ_REQ=$2

DIR_PREFIX='rep'
FILE_PREFIX='fic'
TARGET=''
typeset -i OBJ_COUNT

[ ! -d $ROOT_TREE ] && mkdir $ROOT_TREE

while [ $OBJ_COUNT -ne $OBJ_REQ ]
do
  buildObject
done

treeStatus

# eof
